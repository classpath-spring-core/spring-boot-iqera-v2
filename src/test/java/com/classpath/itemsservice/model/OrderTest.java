package com.classpath.itemsservice.model;

import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import static org.junit.jupiter.api.Assertions.*;

public class OrderTest {

    @Test
    public void testConstructor(){
        Order order = Order.builder().id(12).orderDate(LocalDate.now()).price(12000).build();
        assertNotNull(order);
        assertEquals(order.getId(), 12);
    }

}