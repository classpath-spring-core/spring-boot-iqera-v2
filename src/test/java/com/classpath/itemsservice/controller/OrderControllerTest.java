package com.classpath.itemsservice.controller;

import com.classpath.itemsservice.model.Order;
import com.classpath.itemsservice.service.OrderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;


@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)
public class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrderService orderService;

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private WebApplicationContext context;


    @Before
    public void bootstrapMvc(){
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    @Test
    @WithMockUser(username = "kiran", password = "dummy", roles = "USER")
    public void testFetchAllOrders() throws Exception {
        //set the expectations on the service
        List<Order> orderList = Arrays.asList(Order.builder().id(12).orderDate(LocalDate.now()).build());
        when(orderService.listAll()).thenReturn(new HashSet<>(orderList));

        mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/orders")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200));
    }
}