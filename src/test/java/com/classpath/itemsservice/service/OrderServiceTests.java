package com.classpath.itemsservice.service;

import com.classpath.itemsservice.dao.OrderRepository;
import com.classpath.itemsservice.model.Order;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.time.LocalDate;
import java.util.Optional;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;
import org.junit.runner.RunWith;

@RunWith(SpringJUnit4ClassRunner.class)
public class OrderServiceTests {

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderServiceImpl orderService;


    private Order order;

    @BeforeEach
    public void setUp(){
        this.order = Order.builder().price(15000).orderDate(LocalDate.now()).build();
    }

    @Test
    public void testSaveOrder(){


        System.out.println(orderRepository);
        //set the expectations on the stub
        when(orderRepository.save(order)).thenReturn(Order.builder().price(15000).orderDate(LocalDate.now()).id(12).build());

        //execution
        Order savedOrder = orderService.saveOrder(order);

        //Assertions
        assertNotNull(savedOrder);
        assertEquals(savedOrder.getId(), 12);

        //verify the invocation
        verify(orderRepository, times(1)).save(order);
    }


    @Test
    public void fetchOrderById(){
        //expectations
        when(orderRepository.findById(anyLong())).thenReturn(Optional.of(Order.builder().price(15000).orderDate(LocalDate.now()).build()));

        Order order = orderService.findOrderById(12);

        assertNotNull(order);
        assertEquals(order.getPrice(), 15000);

        //verify
        verify(orderRepository,atMostOnce()).findById(12L);
    }

    @Test
    public void fetchOrderByInvalidId(){
        //expectations
        when(orderRepository.findById(anyLong())).thenReturn(Optional.empty());

        try {
            Order order = orderService.findOrderById(12);
            fail("OrderService did not throws exception");
        } catch (Exception e){
            Assert.assertTrue(e instanceof IllegalArgumentException);
        }
        //verify
        verify(orderRepository,atMostOnce()).findById(12L);
    }

    @Test
    public void fetchOrderByValidId(){
        //expectations
        when(orderRepository.findById(anyLong())).thenReturn(Optional.of(Order.builder().price(15000).orderDate(LocalDate.now()).build()));

        try {
            Order order = orderService.findOrderById(12);
        } catch (Exception e){
            fail("This invocation should not throw Exception");
            Assert.assertTrue(e instanceof IllegalArgumentException);
        }
        //verify
        verify(orderRepository,atMostOnce()).findById(12L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void fetchInValidFetchId(){
        //expectations
        when(orderRepository.findById(anyLong())).thenReturn(Optional.empty());
        Order order = orderService.findOrderById(12);
        //verify
        verify(orderRepository,atMostOnce()).findById(12L);
    }

}