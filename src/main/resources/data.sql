insert into users (user_id, email_address, password, username) values (1, 'kiran@gmail.com', '$2a$10$1PzGxZ2mNuYIERdH5OpAgOAAxzNNEeYsF24Cnsrr5jwSY8ufqQK0G', 'kiran');
insert into users (user_id, email_address, password, username) values (2, 'vinay@gmail.com', '$2a$10$1PzGxZ2mNuYIERdH5OpAgOAAxzNNEeYsF24Cnsrr5jwSY8ufqQK0G', 'vinay');

insert into roles (role_id, role_name) values (1, 'ROLE_USER');
insert into roles (role_id, role_name) values (2, 'ROLE_ADMIN');

insert into users_roles(role_id, user_id) values (1, 1),(2, 1), (2,2);