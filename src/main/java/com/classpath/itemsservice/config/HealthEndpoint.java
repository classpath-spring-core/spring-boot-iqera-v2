package com.classpath.itemsservice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Statement;

@Component("dbHealthEndpoint")
public class HealthEndpoint  implements HealthIndicator {
    @Autowired
    private DataSource dataSource;

    private static int couter = 0;

    @Override
    public Health health() {
        try(Connection connection = dataSource.getConnection()){
            Statement statement = connection.createStatement();
            statement.execute("select * from orders");

        } catch (Exception throwables) {
            System.out.println("====================================");

            throwables.printStackTrace();
            return Health.outOfService().withException(throwables).build();
        }
        //test the db
        System.out.println(" Checking the status of DB");
        //test the rest endpoint
        System.out.println("Checking the status of REST endpoints");

        if(++couter >= 4){
            return Health.outOfService().withException(new IllegalArgumentException("Exceeded number of requests")).build();
        }
        return Health.up().build();
    }
}