package com.classpath.itemsservice.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.UUID;

@Configuration
@Slf4j
public class AppConfig {

    @Bean
    public RestTemplate restTemplate(){
        log.info(" Came inside the creation of Rest Template..............");
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getInterceptors().add(new HttpClientInterceptor());
        return  restTemplate;
    }
}

class HttpClientInterceptor implements ClientHttpRequestInterceptor{

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        System.out.println("Came inside the inteceptor method");
        request.getHeaders().add("Authorization", UUID.randomUUID().toString());
        request.getHeaders().add("Allow-access-origin", "*");
        ClientHttpResponse clientHttpResponse = execution.execute(request, body);
        clientHttpResponse.getHeaders().add("Authorization", UUID.randomUUID().toString());
        return clientHttpResponse;
    }
}