package com.classpath.itemsservice.config;

import com.classpath.itemsservice.dao.OrderRepository;
import com.classpath.itemsservice.model.Order;
import com.classpath.itemsservice.model.OrderLineItem;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.HashSet;

@Component
@AllArgsConstructor
@Slf4j
public class BootstrapOrders  implements ApplicationRunner {

    private OrderRepository orderRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info(" ----------------- Initialzing the data store started ---------------------");
        OrderLineItem lineItem1 = new OrderLineItem();
        lineItem1.setPrice(20000);
        lineItem1.setName("Vivo-I");
        lineItem1.setQty(2);

       OrderLineItem lineItem2 = new OrderLineItem();
        lineItem2.setPrice(20000);
        lineItem2.setName("OPPO");
        lineItem2.setQty(12);

        Order order = Order.builder().orderDate(LocalDate.now()).id(11).price(20000).lineItems(new HashSet<>()).build();

        order.addLineItem(lineItem1);
        order.addLineItem(lineItem2);

        this.orderRepository.save(order);
        log.info(" ----------------- Initialzing the data store completed ---------------------");
    }
}