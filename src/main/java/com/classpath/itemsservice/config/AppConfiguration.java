package com.classpath.itemsservice.config;

import com.classpath.itemsservice.CustomConditionalConfiguration;
import lombok.*;
import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.boot.system.JavaVersion;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

import static org.springframework.boot.system.JavaVersion.EIGHT;
import static org.springframework.boot.system.JavaVersion.FIFTEEN;

//@Configuration
//@Conditional(CustomConditionalConfiguration.class)
//@ConditionalOnJava(FIFTEEN)
public class AppConfiguration {

    @Bean("ItemBean")
    public Item item(){
        return new Item();
    }

    @Bean("OrderBean")
    public Order order(){
        return new Order();
    }
    /*
    //@ConditionalOnJava(EIGHT)
    @ConditionalOnMissingClass(value="com.test")
    @Bean("OrderBean")
    public Order orderBean(){
        return new Order();
    }


    @ConditionalOnClass(name="mysqlDriver")
    @Bean
    public DataSource dataSource(){
        return  null;
    }

    @ConditionalOnBean(name="datasource")
    @Bean
    public TransactionManager transactionManager(){
        return null;
    }

    @ConditionOnClass(name="org.springframework.webmvc.DispatcherServlet")
    @Bean
    public ViewResolver viewResolver(){
        return null;
    }

    @ConditionalOnBean(name="viewResolver")
    @Bean
    public DispatcherServlet dispatcherServlet(){
        return null;
    }
*/
}

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = "id")
class Item {
    private long id;
    private String name;
    private double price;
}

class Order {

}