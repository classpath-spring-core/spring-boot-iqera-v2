package com.classpath.itemsservice.service;

import com.classpath.itemsservice.model.Order;

import java.util.Set;

public interface OrderService {
    Order saveOrder(Order order);

    Set<Order> listAll();

    Order findOrderById(long orderId);

    void deleteOrderById(long orderId);
}