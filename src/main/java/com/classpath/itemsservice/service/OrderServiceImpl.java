package com.classpath.itemsservice.service;

import com.classpath.itemsservice.dao.OrderRepository;
import com.classpath.itemsservice.model.Order;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@Transactional
@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;

    @Override
    public Order saveOrder(Order order) {
        return this.orderRepository.save(order);
    }

    @Override
    public Set<Order> listAll() {
        log.info("Inside the list all method "+this.orderRepository.findAll().size());
        return new HashSet<>(this.orderRepository.findAll());
    }

    @Override
    public Order findOrderById(long orderId) {
        return this.orderRepository.findById(orderId).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public void deleteOrderById(long orderId) {
        this.orderRepository.deleteById(orderId);
    }
}