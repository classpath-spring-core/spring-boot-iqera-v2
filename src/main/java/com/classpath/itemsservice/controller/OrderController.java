package com.classpath.itemsservice.controller;

import com.classpath.itemsservice.model.Order;
import com.classpath.itemsservice.service.OrderService;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("api/v1/")
@AllArgsConstructor
@Slf4j
@Api("Order-API - V1")
public class OrderController {

    private OrderService orderService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private RestTemplate restTemplate;


    @GetMapping("/beans")
    @ApiOperation(value = "To test a Hello world")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Hello World Message"),
            @ApiResponse(code = 404, message = "Not Found")
    })
    public List<String> helloworld(){
        return Arrays.asList(this.applicationContext.getBeanDefinitionNames());
    }

    @PostMapping("/orders")
    @ApiOperation(value = "To save the Order")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Order is successfully created"),
            @ApiResponse(code = 404, message = "Invalid URL"),
            @ApiResponse(code = 403, message = "Forbidden")
    })
    public Order saveOrder(@RequestBody @Valid @ApiParam("Order entity to be saved") Order order){
        log.info("Saving the order ");
        order.getLineItems().forEach(oli-> oli.setOrder(order));
        return this.orderService.saveOrder(order);
    }

    @GetMapping("/orders")
    public Set<Order> getOrders(){
       log.info("=====================");
        System.out.println(" ---> "+this.orderService.listAll().size());
        log.info("=====================");
        return this.orderService.listAll();
    }

    @GetMapping("/order/{id}")
    public Order getOrderById(@PathVariable("id") long orderId){
        return this.orderService.findOrderById(orderId);
    }

    @GetMapping("/courses")
    public String fetchCourses(){
        ResponseEntity<String> response = this.restTemplate.getForEntity("https://my-json-server.typicode.com/prashdeep/courseflix/courses/", String.class);
        return response.getBody();
    }

//    @ExceptionHandler(RuntimeException.class)
//    public ResponseEntity<String> handleRuntimeException(RuntimeException e){
//        return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
//    }
}