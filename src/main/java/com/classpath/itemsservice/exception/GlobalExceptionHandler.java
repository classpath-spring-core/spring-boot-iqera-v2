package com.classpath.itemsservice.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public final ResponseEntity<ErrorResponse> handleRuntimeException(Exception e, WebRequest request){
        return new ResponseEntity<>(new ErrorResponse(200,"Invalid order Id passed"), HttpStatus.BAD_REQUEST);
    }
}

