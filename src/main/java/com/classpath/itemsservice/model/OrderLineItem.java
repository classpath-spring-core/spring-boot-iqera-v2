package com.classpath.itemsservice.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name="line_items")
@NoArgsConstructor
public class OrderLineItem {

    @Id
    @GeneratedValue(strategy = AUTO)
    @Setter @Getter
    private long orderItemId;

    @Setter @Getter
    private int qty;

    @Setter @Getter
    @Max(25000)
    @Min(12000)
    private double price;

    @Setter @Getter
    @NotEmpty(message = "Item name cannot be empty")
    private String name;


    @ManyToOne
    @JoinColumn(name = "order_id")
    @Setter
    private Order order;


}