package com.classpath.itemsservice.model;

import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.Max;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name = "orders")
@Setter @Getter @ToString @Builder @EqualsAndHashCode(of = "id")
@NoArgsConstructor @AllArgsConstructor
public class Order implements Serializable {
    @Id
    @GeneratedValue(strategy = AUTO)
    private long id;

    @Max(20000)
    private double price;

    private LocalDate orderDate;

    @OneToMany(mappedBy = "order", cascade = ALL, fetch = EAGER)
    private Set<OrderLineItem> lineItems = new HashSet<>();

    //scaffolding code
    public void addLineItem(OrderLineItem lineItem){
        this.lineItems.add(lineItem);
        lineItem.setOrder(this);
    }
}