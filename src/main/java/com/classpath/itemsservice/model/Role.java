package com.classpath.itemsservice.model;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.util.Set;
import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name="roles")
@Getter @Setter
public class Role {

    @Id
    @GeneratedValue(strategy = AUTO)
    private int roleId;

    private String roleName;

    @ManyToMany()
    @JoinTable( name = "users_roles",
            joinColumns = @JoinColumn(name="role_id"),
            inverseJoinColumns = @JoinColumn(name="user_id")
    )
    private Set<User> users;
}